module Web.View.Posts.Show where
import           Web.View.Prelude

data ShowView = ShowView { post :: Include "comments" Post }

instance View ShowView where
    html ShowView { .. } = [hsx|
        {breadcrumb}
        <h1>{get #title post}</h1>
        <div>{get #createdAt post |> timeAgo}</div>
        <p>{get #body post}</p>
        <a href={NewCommentAction (get #id post)}>Add comment</a>
        <div>{get #comments post} </div>

    |]
        where
            breadcrumb = renderBreadcrumb
                            [ breadcrumbLink "Posts" PostsAction
                            , breadcrumbText "Show Post"
                            ]
